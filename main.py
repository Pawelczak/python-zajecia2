'''
a=4
b='6'
c = `a` + b
print c
'''
'''
print 2.5
print 2e+4
print 4.0/3.0
print 4.0//3.0   #Dzielenie bez reszty ucina czesc dziesietna.
print 3.5e+4+1000000L*2


print -2+3j
print 3j**2     #Potegowanie
print 3.2+400L/(2+2j)



a,b,c,d,e=1,3,7,4,6
a+=2
b-=2
c*=2
d/=2
print a,b,c,d,e

# imie = raw_input("Jak masz na imie?\n")
# print imie

napis = "Wiek: "+str(18)
print napis
print napis.replace("W","WWW")
print napis.lower()
print napis.upper()
napis="Ta liczba %f to %s" % (3.23, "liczba")
print napis
print '{0}, {1}, {2}'.format('a','b','c')

# LISTY MOZNA ZMIENIAC KROTKI NIE !
# LISTA
l=[1,2,'element',3.14]
print l
#Krotka
k=(1,2,'element',3.14)
print k
print l[1]
print k[1]
print l[1:3]
print ""
print k[:-1] #Od tylu ?
print ""
print 1 in l
l[0] = 3
print l

l[1:3]=['a','b']
print l


#listy jednowymiarowe

macierz=[1,[1,2,3,4]]
print macierz

print macierz[1][3]

macierz[1][3] = 5
print macierz

#List modyfikowanie
lista=[1,2,3]
lista2=lista+[4,5,6]
print lista2

lista=[4,6,2,4,8]
lista.sort()
print lista
lista.reverse()
print lista
lista.append(6)
print lista
lista.insert(2,10)
print lista
lista.pop()
print lista

lista.remove(4) #to wartosc nie indeks
del lista[1:3] #usunie el o indeksie 1 i 2
print lista

#Slowniki
slownik={'a':'b', 'klucz':15,3:[1,3,4]}
print slownik

print slownik['a']

print len(slownik)

print list(slownik.keys())

d={}

#print 'b' in d

d['b'] = 1

print d

del slownik[3]
print slownik

slownik.clear()
print slownik



a=True
b=False
print a,b

x=4
y=8

print x<y
print x==y
print x!=y

print a or b
print a and b
print not a
print not b

#operatory
a,b = 2,3

print a+b
print a-b
print a*b
print a/b
print a%b
print a**b





'''
'''
a = 6
b = 5
if a < 4:
    print a
elif a > 4:
    print b
for x in range (-10,11):
    print "%+i" % x,

print "\n"

for x in range(5,100,10):# ostatnia wartosc to krok
    print "%3i%6o%5x" % (x,x,x)

for x in range(5,100,10):# ostatnia wartosc to krok
    print "%-3i%#-6o%#-5x" % (x,x,x) # # to odstep

for x in range(5,100,10):# ostatnia wartosc to krok
    print "%3i %#04o %#04x" % (x,x,x) # # to odstep

a=[1,2,3,4,5,6]
while a:
    a=a[:len(a)-1]
    print a
'''


