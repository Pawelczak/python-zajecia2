class AdresRekord:
    id = 0
    def __init__(self,imie,nazwisko,ulica,kodPocztowy,miasto):
        self.i = imie
        self.n = nazwisko
        self.u = ulica
        self.kP = kodPocztowy
        self.m = miasto
        self.userId = AdresRekord.id
        AdresRekord.id += 1
    def wyswietlRekord(self):
        print '%-15s%-15s%-15s%-15s%-15s%s' % (self.userId.__str__(),self.i,self.n,self.u,self.kP,self.m)


Adresy = []
wejscie = "start"
while wejscie != "0":
    wejscie = raw_input("Co chcesz zrobic ? d - dodaj nowy adres, w - wyswietl adresy, e - edytuj adres, u - usun adres, 0 - zakoncz program\n")
    if wejscie == "d":
        imie = raw_input("Podaj imie\n")
        nazwisko = raw_input("Podaj naziwsko\n")
        ulica = raw_input("Podaj Ulice\n")
        kodPocztowy = raw_input("Podaj kod pocztowy\n")
        miasto = raw_input("Podaj miasto\n")
        Adresy.append(AdresRekord(imie,nazwisko,ulica,kodPocztowy,miasto))
        print "Id tego wpisu to " + (AdresRekord.id-1).__str__()
    if wejscie == "w":
        print '%-15s%-15s%-15s%-15s%-15s%s' % ('Id','Imie','Nazwisko','Ulica','Kod pocztowy','Miasto')
        for x in Adresy:
            x.wyswietlRekord()
    if wejscie == "e":
        podanyUserId = int(raw_input("Podaj Id wpisu\n"))
        znaleziony = False
        for x in Adresy:
            if x.userId == podanyUserId:
                znaleziony = True
                wyborEdycji = "brak"
                while wyborEdycji != "0":
                    print "Co chcesz edytowac ?\n"
                    print "n - nazwisko\n"
                    print "u - ulica\n"
                    print  "k - kod pocztowy\n"
                    print  "m - miasto\n"
                    print  "0 - koniec edycji\n"
                    wyborEdycji = raw_input()
                    if wyborEdycji == "n":
                        x.n = raw_input("Podaj nowe nazwisko")
                    if wyborEdycji == "u":
                        x.u  = raw_input("Podaj nowa ulice")
                    if wyborEdycji == "k":
                        x.kP = raw_input("Podaj nowy kod pocztowy")
                    if wyborEdycji == "m":
                        x.m =  raw_input("Podaj nowe miasto")
        if znaleziony == False:
            print "Nie znaleziono wpisu o takim id"
    if wejscie == "u":
        podanyUserId = int(raw_input("Podaj Id wpisu do usuniecia\n"))
        znaleziony = False
        for x in Adresy:
            if x.userId == podanyUserId:
                znaleziony = True
                Adresy.remove(x)
        if znaleziony == False:
            print "Nie znaleziono wpisu o takim id"

