def sito(n):
    boole = [True] * n
    for x in range(2,n):
        if boole[x] == True:
            temp = x*x
            multiply = 1
            while temp<n:
                boole[temp] = False
                temp = temp + x * multiply
                multiply *= 1
    poz = 0
    count = 0
    line = ""
    for x in boole:
        if x == True:
            if poz >= 2:
                line += "\t\t" +poz.__str__()
                count += 1
        poz += 1
        if count > 0 and count % 8 == 0:
            print line
            line = ""
            count = 0
    print line
print ""
sito(200)